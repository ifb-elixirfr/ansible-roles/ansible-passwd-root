# Password role management

* Manage root password


## Root password

If `rootpw_crypted` is defined, manage the root password.

The variable `rootpw_crypted` is the SHA-512 password hash generated using the following commands:
```
python -c 'import crypt,getpass;pw=getpass.getpass();print(crypt.crypt(pw) if (pw==getpass.getpass("Confirm: ")) else exit())'
```

Documentation ansible "Generate crypted passwords": https://docs.ansible.com/ansible/latest/reference_appendices/faq.html#how-do-i-generate-crypted-passwords-for-the-user-module
